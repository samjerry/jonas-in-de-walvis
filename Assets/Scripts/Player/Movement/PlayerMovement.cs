﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [SerializeField]
    private float _speed = 10f;
    [SerializeField]
    public float jumpHeight = 15f;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;

    public bool grounded;
    private bool doubleJumped;
    private bool canWalk = true;

    private float _bounceBoost = 30.0f;

    private InputManager im;
    Rigidbody2D _rb;

	// Use this for initialization
	void Start ()
    {
        _rb = GetComponent<Rigidbody2D>();
        im = GetComponent<InputManager>();
    }

	
	// Update is called once per frame
	void Update ()
    {
        Walking();
        Jumping();
        OffBorder();
	}

    void FixedUpdate()
    {

        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

    // Walking
    void Walking()
    {
         if (im.Left())
         {
             transform.Translate(new Vector2(-_speed * Time.deltaTime, 0));
         }
         else if (im.Right())
         {
             transform.Translate(new Vector2(_speed * Time.deltaTime, 0));
         }
    }

    // Jumping
    void Jumping()
    {
        if (grounded)
            doubleJumped = false;

        if (im.Jump() && grounded)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, jumpHeight);
        }

        if (im.Jump() && !doubleJumped && !grounded)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, jumpHeight);
            doubleJumped = true;
        }
    }

   

    // Collision
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bounce")
        {
            _rb.AddForce(Vector2.up * _bounceBoost, ForceMode2D.Impulse);
        }
    }


    void OffBorder()
    {
        Vector2 playerPos = transform.position;

        playerPos.x = Mathf.Clamp(transform.position.x, -22, 22);
        //playerPos.y = Mathf.Clamp(transform.position.y, -12, 11);

        transform.position = playerPos;
    }
}