﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [SerializeField]
    private float _speed = 10f;
    [SerializeField]
    private float _jumpSpeed = 10f;
    [SerializeField]
    private float fallMultiplier = 2.5f;

    private bool isGrounded;
    private bool isJumping = false;

    Rigidbody2D _rb;

	// Use this for initialization
	void Start ()
    {
        _rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Walking();
        Jumping();
        Debug.Log(isJumping);
	}

    void Walking()
    {
            if (Input.GetKey(KeyCode.A))
            {
                _rb.velocity += Vector2.left * _speed * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.D))
            {
                _rb.velocity += Vector2.right * _speed * Time.deltaTime;
            }
    }

    void Jumping()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            if (isGrounded && _rb.velocity.y == 0)
            {
                _rb.velocity += Vector2.up * _jumpSpeed;
            }

        }

        // this code causes the player to fall faster over time
        if (_rb.velocity.y < 0)
        {
            _rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }

    }

    void OnCollisionEnter2D (Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground")
            isGrounded = false;

    }
}
