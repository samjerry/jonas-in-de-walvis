﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour{

    void OnTriggerEnter2D(Collider2D col){
        if (col.transform.tag == "Player"){
            Next();
            Debug.Log("Hoi");
        }
    }

    void Next(){
        SceneManager.LoadScene(SceneManager.sceneCount + 1);
    }
}
