﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidSpawner : MonoBehaviour {

	[SerializeField]private float SpawnDelay;
	[SerializeField]private int AcidAmount;
	[SerializeField]private GameObject[] AcidTypes;
	private GameObject SpawnedType;

	void Start () {
		InvokeRepeating ("SpawnAcid", 0.0f, SpawnDelay);
	}	

	public void SpawnAcid(){
		Debug.Log ("started spawning acid");
		for (int i = 0; i < AcidAmount; i++) {
			Vector3 SpawnPos = new Vector3 (Random.Range(-3,3), this.transform.position.y);
			SpawnedType = AcidTypes[Random.Range(0, AcidTypes.Length)];
			Instantiate (SpawnedType, SpawnPos, Quaternion.identity);
		}
	}
}
